# Visual Word Embeddings
With our approach the text become an image as we can see in the figure.

![example text to image](imgs/w2v2imgsample.jpg)

In this example, the word "pizza" is encoded into a visual word <img src="https://latex.codecogs.com/gif.latex?\hat{t}_k " />  based on Word2Vec feature vector with length 15. 
This visual word can be transformed into different shapes, varying the V parameter (in this example <img src="https://latex.codecogs.com/gif.latex?V = 2,3,6 " /> superpixels)


## Usage
To use this project you shoud do many steps:
* starting from a dataset with this shape:
  * /train/class/sample1.csv
  * /test/class/sample2.csv
* text data preprocessing pipeline
  * train w2v
  * transform all text in embeddings
  * create the images dataset
* Classification
  * train the model

### Text data preprocessing pipeline
* requirements:
  * gcc
  * python:
    * opencv-python
    * numpy
* commands
```bash
cp -r ./text_preprocessing/word2vec /dataset/folder
cp ./text_preprocessing/02-create_w2v_features.sh /dataset/folder
cp ./text_preprocessing/txt2img_converter.py /dataset/folder
cp ./text_preprocessing/03-create-txt2img-dataset.sh /dataset/folder

cd /dataset/folder
./02-create_w2v_features.sh
./03-create-txt2img-dataset.sh
```

### Classification
* requirements: 
  * python:
    * tensorflow/tensorflow-gpu
* commands
```bash
# edit in keras_classifier.py "train_path = ..." and "test_path = ..."
python keras_classifier.py
```

## Results
Can you find all results in the [paper](http://artelab.dista.uninsubria.it/res/research/papers/2021/2021_ICPR_W_Gallo_Visual.pdf).

## Citations
```
@INPROCEEDINGS{Gallo:2021:ICPR, 
   author={Ignazio Gallo, Shah Nawaz, Nicola Landro, Riccardo La Grassa}, 
   booktitle={Pattern Recognition. ICPR International Workshops and Challenges (ICPR2021)}, 
   title={Visual Word Embedding for Text Classification}, 
   year={2021}, 
   month={Feb}, 
   pages={339--352},
}
```
