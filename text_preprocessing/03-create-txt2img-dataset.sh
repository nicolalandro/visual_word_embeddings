#!/usr/bin/env bash

for visualword_cell_w in 4
do
for sp in 4
do
for space_bw_words in 16
do
for w2vfeat in 36
do

dstdir="space${space_bw_words}-sp${sp}x${sp}-w2v${w2vfeat}feat-visualword_w${visualword_cell_w}"
w2v_features="word2vec/vectors-${w2vfeat}features.txt"

# remove dir if exists
if [[ -d ${dstdir} ]]; then
   rm -rf ${dstdir}
fi
mkdir ${dstdir}
echo "created dir ${dstdir}"

echo "Working on train..."
dst_sub_dir='train'
srcdir='train'

for d in `ls -d -w 1 ${srcdir}/*`
do
  if [[ -d ${d} ]]; then
    echo "Processing category ${d}"
    #echo "python txt2img_converter.py ${dst_sub_dir} ${d} ${dstdir} ${w2v_features} ${sp} ${space_bw_words} ${visualword_cell_w}"
    python txt2img_converter.py ${dst_sub_dir} ${d} ${dstdir} ${w2v_features} ${sp} ${space_bw_words} ${visualword_cell_w}
  fi
done


echo "Working on test..."
dst_sub_dir='test'
srcdir='test'

for d in `ls -d -w 1 ${srcdir}/*`
do
  if [[ -d ${d} ]]; then
    echo "Processing category ${d}"
    #echo "python txt2img_converter.py ${dst_sub_dir} ${d} ${dstdir} ${w2v_features} ${sp} ${space_bw_words} ${visualword_cell_w}"
    python txt2img_converter.py ${dst_sub_dir} ${d} ${dstdir} ${w2v_features} ${sp} ${space_bw_words} ${visualword_cell_w}
  fi
done

done # sp
done # space_bw_words
done # w2vfeat
done # visualword_cell_w
