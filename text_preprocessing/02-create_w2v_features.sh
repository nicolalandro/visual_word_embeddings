#this function will convert text to lowercase and will disconnect punctuation and special symbols from words
function normalize_text {
  awk '{print tolower($0);}' < $1 | sed -e 's/\./ \. /g' -e 's/<br \/>/ /g' -e 's/"/ " /g' \
  -e 's/,/ , /g' -e 's/(/ ( /g' -e 's/)/ ) /g' -e 's/\!/ \! /g' -e 's/\?/ \? /g' \
  -e 's/\;/ \; /g' -e 's/\:/ \: /g' > $1.norm
}

cd word2vec
gcc word2vec.c -o word2vec -lm -pthread -O3 -march=native -funroll-loops

# combining *.txt into one file
find ../train/ -type f -name '*.csv' -exec cat {} + >> alldata.txt
find ../test/ -type f -name '*.csv' -exec cat {} + >> alldata.txt
#cat ../20news-bydate-train/train-docs.csv ../20news-bydate-test/test-docs.csv > alldata.txt

normalize_text alldata.txt

for f in 36 24 12
do
   echo "creating vectors-${f}features.txt..."
   time ./word2vec -train "alldata.txt.norm" -output "vectors-${f}features.txt" -cbow 0 -size ${f} -window 10 -negative 5 -hs 0 -sample 1e-4 -threads 40 -binary 0 -iter 20 -min-count 1 -sentence-vectors 1
done

