import glob, os
import numpy as np
import sys
import math
import cv2
import re

# from itertools import izip_longest


# NUM_WORDS = 20


def array2color(rgb):
    return "#{0:02x}{1:02x}{2:02x}".format(int(rgb[0]), int(rgb[1]), int(rgb[2]))


def extract_formatted_string(text, dictionary):
    text = re.sub('[^a-zA-Z0-9]+', ' ', text).lower()
    words = text.split(' ')
    row_features = np.array([])
    for word in words:
        # TODO: remove stop words
        # ...
        features = dictionary.get(word, None)
        if features is not None:
            row_features = np.append(row_features, features)

    return row_features


def modify_superpixel_size(sp_i, visualword_cell_w, superpixel_h):
    val = 0
    b = 1 # superpixel_h / 2.0
    a = -b / ((visualword_cell_w - 1) / 2.0)
    val = math.fabs(a * sp_i + b)
    return int(val)

def main():
    sub_dir = sys.argv[1]  # 20news-bydate-test
    txt_dir = sys.argv[2]  # 20news-bydate-test/talk.religion.misc
    dst_base_dir = sys.argv[3]  # '/home/super/datasets/7pixel/image-offer/ferramenta/color2-dataset'
    w2v_features = sys.argv[4]  # '/home/super/PycharmProjects/txt2image/10classes_3feat/word2vec/vectors-52cls.txt'
    superpixel_w = int(sys.argv[5])  # 8
    superpixel_h = int(sys.argv[5])  # 8
    separator = int(sys.argv[6])  # 4
    visualword_cell_w = int(sys.argv[7]) # num superpixel per row

    sp_diff_size = False
    image_w = 256  # pixel
    image_h = 256  # pixel


    if not os.path.exists(dst_base_dir):
        os.mkdir(dst_base_dir)

    if not os.path.exists(dst_base_dir + '/' + sub_dir):
        os.mkdir(dst_base_dir + '/' + sub_dir)

    # read color encoder
    dictionary = {}

    with open(w2v_features, 'r') as file:
        # reader = csv.reader(file, delimiter=' ')
        row1 = next(file).split(' ')  # gets the first line
        num_words = row1[0]
        num_features = int(row1[1])
        assert (num_features % 3 == 0), 'it is better to use a Word2Vec feature vector divisible by 3'

        features_max = np.full(num_features, float("-inf"))
        features_min = np.full(num_features, float("inf"))

        for row in file:
            row = row.split(' ')
            features = np.zeros(num_features)
            for sp_i in range(0, len(features)):
                # row[0] is the word
                features[sp_i] = row[sp_i + 1]
                if features[sp_i] > features_max[sp_i]:
                    features_max[sp_i] = features[sp_i]
                if features[sp_i] < features_min[sp_i]:
                    features_min[sp_i] = features[sp_i]

            dictionary[row[0]] = features
            # print dictionary

        assert (int(num_words) == len(dictionary))

        # normalization
        for key, value in dictionary.iteritems():
            y = (value - features_min) / (features_max - features_min) * 255
            dictionary[key] = y

    num_superpixels = num_features / 3
    #visualword_cell_w = num_features / 3  # num superpixel
    visualword_cell_h = int(math.ceil(num_superpixels / float(visualword_cell_w)))  # num superpixel


    csv_file = dst_base_dir + "/" + sub_dir + '.csv'
    with open(csv_file, 'a') as txt_outfile:
        for f in os.listdir(txt_dir):
            txt_file = os.path.join(txt_dir, f)
            # open text document for reading
            with open(txt_file, 'r') as file:
                # tranform all the words in features
                doc_features = np.array([])
                identifier = os.path.basename(txt_file)
                head, cls = os.path.split(os.path.split(txt_file)[0])
                for sp_i, row in enumerate(file):
                    np_features = extract_formatted_string(row, dictionary)
                    doc_features = np.append(doc_features, np_features)

                if not os.path.exists(dst_base_dir + '/' + sub_dir + '/' + cls):
                    os.mkdir(dst_base_dir + '/' + sub_dir + '/' + cls)

                # create image
                img = np.zeros((image_h, image_w, 3), np.uint8)

                word_id = 0

                x = separator
                y = separator

                assert separator + visualword_cell_w * superpixel_w <= image_w, 'the image width is smaller than the visual word width'
                assert separator + visualword_cell_h * superpixel_h <= image_h, 'the image height is smaller than the visual word height'

                while True:
                    ptl = word_id * num_features
                    ptr = (word_id + 1) * num_features
                    word_features = doc_features[ptl:ptr]

                    sp_i = 0  # superpixel index

                    if x + visualword_cell_w * superpixel_w + separator > image_w:
                        x = separator
                        y = y + separator + visualword_cell_h * superpixel_h
                    if y + visualword_cell_h * superpixel_h + separator > image_h:
                        break

                    word_id += 1

                    # print "x=", x, " y=", y

                    # write one word
                    for row in xrange(y, y + visualword_cell_h * superpixel_h, superpixel_h):
                        spw = 0 # counter for superpixels in row
                        for col in xrange(x, x + visualword_cell_w * superpixel_w, superpixel_w):
                            ptl = sp_i * 3
                            ptr = (sp_i + 1) * 3
                            bgr = word_features[ptl:ptr]
                            if len(bgr) < 3:
                                break

                            row_incr = 0
                            if sp_diff_size:
                                row_incr = modify_superpixel_size(spw, visualword_cell_w, superpixel_h)
                            # write superpixel
                            row_start = row + row_incr
                            row_end = row + superpixel_w
                            # if sp_i >= visualword_cell_w :
                            #     row_start = row
                            #     row_end = row + superpixel_w - row_incr
                            for srow in range(row_start, row_end):
                                for scol in range(col, col + superpixel_h):
                                    # mat[i][j] = 1
                                    img[srow, scol] = bgr

                            sp_i += 1
                            spw += 1

                            # cv2.imwrite(dst_base_dir + '/' + sub_dir + '/' + cls + '/' + identifier + '.png', img)
                            # raw_input("Press Enter to continue...")

                    x += visualword_cell_w * superpixel_w + separator

                cv2.imwrite(dst_base_dir + '/' + sub_dir + '/' + cls + '/' + identifier + '.png', img)



if __name__ == '__main__':
    main()
