import os
import tensorflow as tf
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator

model_names = ["InceptionResNetV2", "InceptionV3", "MobileNet"]
model_name = model_names[0]
gpu = "1"
os.environ["CUDA_VISIBLE_DEVICES"] = gpu

dataset_name = "AGnews"
train_path = '/home/risen/datasets-nas/%s/space16-sp4x4-w2v36feat-visualword_w4/train' % dataset_name
test_path = '/home/risen/datasets-nas/%s/space16-sp4x4-w2v36feat-visualword_w4/test' % dataset_name
# train_path = '/home/risen/datasets-nas/cifar10/train'
# test_path = '/home/risen/datasets-nas/cifar10/test'
checkpoint_filepath = '/tmp/checkpoint/%s_%s_gpu%s.hdf5' % (model_name, dataset_name, gpu)
log_file = '%s_%s_gpu%s.log' % (model_name, dataset_name, gpu)
restore_model_file_path = None
initial_epoch = 0

batch_size = 32
epochs = 300
image_size = 256
channel = 3

checkpoint_dir = os.path.dirname(checkpoint_filepath)
if not os.path.exists(checkpoint_dir):
    os.mkdir(checkpoint_dir)

datagen = ImageDataGenerator()
train_it = datagen.flow_from_directory(train_path,
                                       class_mode='categorical',
                                       batch_size=batch_size,
                                       target_size=(image_size, image_size),
                                       color_mode='rgb', )
test_it = datagen.flow_from_directory(test_path,
                                      class_mode='categorical',
                                      batch_size=batch_size,
                                      target_size=(image_size, image_size),
                                      color_mode='rgb', )

num_classes = train_it.num_classes

input_shape = (image_size, image_size, channel)
if model_name == "InceptionResNetV2":
    base_model = tf.keras.applications.InceptionResNetV2(
        include_top=False,
        weights="imagenet",
        input_tensor=None,
        input_shape=None,
        pooling=None,
    )
elif model_name == "InceptionV3":
    base_model = tf.keras.applications.InceptionV3(
        include_top=False,  # no classification layer
        weights="imagenet",    # weights=None,
        input_tensor=None,
        input_shape=input_shape,
        pooling=None,  # no avg pool at the end
    )
elif model_name == "MobileNet":
    base_model = tf.keras.applications.MobileNet(
        include_top=False,  # no classification layer
        weights="imagenet",  # weights=None,
        input_tensor=None,
        input_shape=input_shape,
    	pooling=None,  # no avg pool at the end
    )
else:
    print("unknown model:", model_name)
    exit()

inputs = tf.keras.Input(shape=input_shape)
x = base_model(inputs, training=True)
x = tf.keras.layers.GlobalAveragePooling2D()(x)
outputs = tf.keras.layers.Dense(num_classes, activation='softmax')(x)

model = tf.keras.Model(inputs, outputs)

model.compile(optimizer=tf.keras.optimizers.Adam(),
              loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
              metrics=[tf.keras.metrics.Accuracy()])

model.summary()

# restore weights
if restore_model_file_path is not None:
    model.load_weights(restore_model_file_path)

model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_filepath,
    save_weights_only=True,
    save_freq='epoch',
    mode='auto',
    save_best_only=False)

csv_logger = tf.keras.callbacks.CSVLogger(log_file, append=True)

callbacks_list = [model_checkpoint_callback, csv_logger]
model.fit_generator(train_it,
                    epochs=epochs,
                    validation_data=test_it,
                    callbacks=callbacks_list,
                    initial_epoch=initial_epoch)
